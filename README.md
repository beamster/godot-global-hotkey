# Godot Global Hotkey

This is a GDExtension (Godot 4.1+) that adds some limited support for getting
input events globally without requiring the window be in immediate focus.

This is useful for hotkey support that will perform actions while focusing on
other programs.

## License

This project copies in some support code from the Godot Engine project itself.
Those files are slightly modified to compile into the environment of the
extension, but are otherwise untouched from the 4.1 Godot engine revision. These
files retain the same license terms as the Godot Engine at the time they were
copied and have the license retained at the tops of those files.

Otherwise, the code of the extension is licensed as MIT.

## OS Support

Currently, Windows is supported natively.

Linux support via Wayland is a challenge due to the security model of Wayland.
There is currently no standard way to get general input events from the OS.
However, some limited support is there in cases where the extension can detect
a compositor with some API means of registering such a callback.

MacOS is not yet supported.

## Usage

Just create an instance of the `GlobalHotkey` class and use the `key_event`
signal to fire an event for every keypress on the system.

```gdscript
var ghk = GlobalHotkey.new()
ghk.key_event.connect(func(event: InputEventKey):
	if event is InputEventKey and event.is_pressed():
		print(event.as_text_keycode())
)
```

To know if global hotkeys are supported on the platform (and have the right
permissions granted, etc), use the `is_supported` method:

```gdscript
var ghk = GlobalHotkey.new()
if not ghk.is_supported():
    print('GlobalHotkey is not available on this platform!')
```

## Building

To build whatever works for the platform you are currently on:

```shell
scons target=template_release
```

To build a version suitable for the debugging environment of Godot:

```shell
scons target=template_debug
```

To build for a particular platform, specify `windows`, `linux`, or `macos`
to the `platform` field:

```shell
scons target=template_release platform=windows
```

To build for a particular version of Godot, specify that as an argument via
the `godot_version` parameter:

```shell
scons target=template_release godot_version=4.1
```

By default, the script will target the latest Godot release. This extension
is not necessarily compatible with all releases.
