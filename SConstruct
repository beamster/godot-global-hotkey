#!/usr/bin/env python
import re
import os
import sys
import glob

# The name of the module
name = "godot-global-hotkey"

# Add specific variables
# Godot version target
godot_version = ARGUMENTS.get("godot_version", "4.1")
if "godot_version" in ARGUMENTS:
    del ARGUMENTS["godot_version"]

# Sanitize godot_version
if not re.match(r"^\d+[.]\d+$", godot_version):
    print("Error: specified `godot_version` must be in the form major.minor such as '4.1'.")
    Exit(1)

godot_target_major, godot_target_minor = godot_version.split('.')

# Ensure godot-cpp repo is cloned at all
os.system(f"git submodule update --init")
os.system(f"cd godot-cpp; git fetch")

# Use that version of godot
godot_cpp_path = os.path.join(os.path.expanduser('~'), '.cache', f"godot-cpp-{godot_version}")
if not os.path.exists(godot_cpp_path):
    os.system(f"git clone godot-cpp {godot_cpp_path} --shared")
if not os.path.exists(f"godot-cpp-{godot_version}"):
    os.symlink(godot_cpp_path, f"godot-cpp-{godot_version}", target_is_directory=True)
os.system(f"cd {godot_cpp_path}; git checkout godot-{godot_version}-stable")

# Import godot-cpp scons
env = SConscript(f"{godot_cpp_path}/SConstruct")

# Flag the godot version
flags = []

# The targets we are building and any prerequisites
targets = []
deps = []

# For reference:
# - CCFLAGS are compilation flags shared between C and C++
# - CFLAGS are for C-specific compilation flags
# - CXXFLAGS are for C++-specific compilation flags
# - CPPFLAGS are for pre-processor flags
# - CPPDEFINES are for pre-processor defines
# - LINKFLAGS are for linking flags

# tweak this if you want to use different folders, or more folders, to store your source code in.
flags.append(f"-Isrc")
sources = Glob("src/*.cpp")
sources.extend(list(set(Glob("src/**/*.cpp")) - set(Glob("src/platform/**/*.cpp"))))

# Platform specific files
sources.extend(Glob(f"src/platform/{env['platform']}/*.cpp"))
sources.extend(Glob(f"src/platform/{env['platform']}/**/*.cpp"))

env.Append(TARGET_ARCH='i386' if env['platform'].endswith('32') else 'x86_64')

# Add third-party include path
lib_prefix = f"third-party/{env['platform']}-{env['arch']}"
lib_path = lib_prefix + '/lib'
flags.append(f"-I{lib_prefix}/include")

# Add link options
output_path = '#bin/' + env['platform'] + '/'

# Determine where our dependencies are built and installed to
lib_prefix = os.path.join('third-party', env['platform'])
lib_path = lib_prefix + '/lib'

# Build our dependencies
if env['platform'] == 'windows':
    # Nothing to do
    pass

# Add library path
flags.append(f"-L{lib_path}")

# Establish output path
output_path = '#bin/' + env['platform'] + '/'
flags.append(f"-L{output_path}")

# Create gdextension file from template
gdextension = env.Command(target=f'dist/{godot_version}/{name}/{name}.gdextension',
                          source='./module.gdextension',
                          action="sed -e 's/gdexample/" + name + f"/g' -e 's/x[.]x/{godot_version}/g' < $SOURCE > $TARGET")

if env["platform"] == "macos":
    library = env.SharedLibrary(
        "dist/{}/{}/osx/bin/lib{}.{}.{}.framework/lib{}.{}.{}".format(
            godot_version, name,
            name, env["platform"], env["target"],
            name, env["platform"], env["target"]
        ),
        source=sources,
        parse_flags=' '.join(flags),
    )
else:
    library = env.SharedLibrary(
        "dist/{}/{}/{}/{}/lib{}{}{}".format(
            godot_version,
            name,
            env["platform"],
            env["arch"],
            name, env["suffix"], env["SHLIBSUFFIX"]
        ),
        source=sources,
        parse_flags=' '.join(flags),
    )

# Tar/Zip distributions
targets.append(
    env.Zip(f"dist/{name}_godot-{godot_version}", [f"dist/{godot_version}"], ZIPROOT=f"dist/{godot_version}")
)

Default(*deps, library, gdextension, *targets)
