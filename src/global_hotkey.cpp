#include "global_hotkey.hpp"

#include <godot_cpp/variant/utility_functions.hpp>

#include <godot_cpp/classes/input_event_key.hpp>

using namespace godot;

/**
 * Establish methods we expose to GDScript and the Godot UI.
 */
void GlobalHotkey::_bind_methods() {
	// Add key signals
	ClassDB::add_signal("GlobalHotkey", MethodInfo("key_event", PropertyInfo(Variant::OBJECT, "input_event")));
	ClassDB::bind_method(D_METHOD("is_supported"), &GlobalHotkey::is_supported);
}

/**
 * Construct and initialize the class.
 */
GlobalHotkey::GlobalHotkey() {
	this->_supported =
#ifdef _WIN32
		this->_win_init();
#else
		this->_linux_init();
#endif
}

/**
 * Clean up.
 */
GlobalHotkey::~GlobalHotkey() {
#ifdef _WIN32
	this->_win_deinit();
#else
	this->_linux_deinit();
#endif
}

bool GlobalHotkey::is_supported() {
	return this->_supported;
}

void GlobalHotkey::_emit(int keycode, int physical_keycode, bool pressed, bool shift_pressed, bool alt_pressed, bool control_pressed, bool meta_pressed) {
	// Create InputEventKey
	auto key_event = Ref<InputEventKey>(memnew(InputEventKey));
	key_event->set_pressed(pressed);
	key_event->set_keycode((Key)keycode);
	key_event->set_physical_keycode((Key)physical_keycode);
	key_event->set_shift_pressed(shift_pressed);
	key_event->set_ctrl_pressed(control_pressed);
	key_event->set_meta_pressed(meta_pressed);
	key_event->set_alt_pressed(alt_pressed);

	// Call signal
	emit_signal("key_event", key_event);
}

/**
 * Platform specific implementations.
 */
#ifdef _WIN32

#include <vector>
#include <algorithm>

HHOOK _main_keyboard_hook = NULL;
int _keyboard_hook_ref = 0;
std::vector<GlobalHotkey*> _klasses;

// Much of this is then borrowed from platform\windows\display_server_windows.cpp in Godot
LRESULT CALLBACK GlobalHotkey::KeyboardProc(int nCode, WPARAM wParam, LPARAM lParam) {
	if (nCode == HC_ACTION) {
		switch (wParam) {
			case WM_KEYUP:
			case WM_KEYDOWN:
				PKBDLLHOOKSTRUCT p = (PKBDLLHOOKSTRUCT)lParam;

				// Is it pressed?
				bool pressed = wParam == WM_KEYDOWN;

				// Key keycode
				EngineKey keycode = KeyMappingWindows::get_keysym(p->vkCode);
				if ((p->flags & LLKHF_EXTENDED) && (p->vkCode == VK_RETURN)) {
					// Special case for Numpad Enter key.
					keycode = EngineKey::KP_ENTER;
				}

				EngineKey physical_keycode = KeyMappingWindows::get_scansym(p->scanCode, p->flags & LLKHF_EXTENDED);

				// Determine the modifiers
				bool control_pressed = (GetKeyState(VK_LCONTROL) & 0x8000) != 0;
				control_pressed |= (GetKeyState(VK_RCONTROL) & 0x8000) != 0;
				control_pressed |= (GetKeyState(VK_CONTROL) & 0x8000) != 0;
				bool alt_pressed = (GetKeyState(VK_LMENU) & 0x8000) != 0;
				alt_pressed |= (GetKeyState(VK_RMENU) & 0x8000) != 0;
				alt_pressed |= (GetKeyState(VK_MENU) & 0x8000) != 0;
				bool meta_pressed = (GetKeyState(VK_LWIN) & 0x8000) != 0;
				meta_pressed |= (GetKeyState(VK_RWIN) & 0x8000) != 0;
				bool shift_pressed = (GetKeyState(VK_LSHIFT) & 0x8000) != 0;
				shift_pressed |= (GetKeyState(VK_RSHIFT) & 0x8000) != 0;
				shift_pressed |= (GetKeyState(VK_SHIFT) & 0x8000) != 0;

				// Force them to false if they were themselves the keys pressed
				if (keycode == EngineKey::SHIFT) {
					shift_pressed = false;
				}
				if (keycode == EngineKey::ALT) {
					alt_pressed = false;
				}
				if (keycode == EngineKey::CTRL) {
					control_pressed = false;
				}
				if (keycode == EngineKey::META) {
					meta_pressed = false;
				}

				// Send the event with:
				for (GlobalHotkey* _hotkey : _klasses) {
					_hotkey->_emit((int)keycode, (int)physical_keycode, pressed, shift_pressed, alt_pressed, control_pressed, meta_pressed);
				}

				break;
		}
	}

	return CallNextHookEx(_main_keyboard_hook, nCode, wParam, lParam);
}

bool GlobalHotkey::_win_init() {
	if (_main_keyboard_hook == NULL) {
		// Set up the keyboard hook
		// TODO: critical section here
		KeyMappingWindows::initialize();
		_main_keyboard_hook = SetWindowsHookEx(WH_KEYBOARD_LL, KeyboardProc, NULL, 0);
		if (_main_keyboard_hook == NULL) {
			UtilityFunctions::print("GlobalHotkey::_win_init(): Error: cannot create keyboard hook");
			return false;
		}
		_keyboard_hook_ref++;
	}

	_klasses.push_back(this);

	return true;
}

bool GlobalHotkey::_win_deinit() {
	if (_keyboard_hook_ref > 0) {
		_keyboard_hook_ref--;
	}

	if (_keyboard_hook_ref == 0) {
		if (_main_keyboard_hook != NULL) {
			UnhookWindowsHookEx(_main_keyboard_hook);
			_main_keyboard_hook = NULL;
		}
	}

	auto it = std::find(_klasses.begin(), _klasses.end(), this);
	if (it != _klasses.end()) {
		_klasses.erase(it);
	}

	return true;
}
#else
/* Linux implementation */

bool GlobalHotkey::_linux_init() {
	// Determine the compositor we need to communicate with.
	const char *socket_path = getenv("SWAYSOCK");
	if (socket_path) {
		if (this->_sway_init(socket_path)) {
			return true;
		}
	}

	// Could not establish global hotkey
	return false;
}

/**
 * Specifically handle interacting with the Sway compositor.
 */
bool GlobalHotkey::_sway_init(const char* socket_path) {
	int sockfd = socket(AF_UNIX, SOCK_STREAM, 0);
	if (sockfd == -1) {
		// Could not establish a socket
		return false;
	}

	// Create a connection to the local socket
	struct sockaddr_un addr;
	memset(&addr, 0, sizeof(struct sockaddr_un));
	addr.sun_family = AF_UNIX;
	strncpy(addr.sun_path, socket_path, sizeof(addr.sun_path) - 1);

	if (::connect(sockfd, (struct sockaddr *)&addr, sizeof(struct sockaddr_un)) == -1) {
		// Could not connect to the socket
		close(sockfd);
		return false;
	}

	// We are connected... assign our socket
	this->_sway_socket = sockfd;

	// Tell Sway we want input events
	const char *message = "[\"input\"]";
	uint32_t length = strlen(message);
	uint32_t payload_length = sizeof(IPC_MAGIC) - 1 + sizeof(uint32_t) * 2 + length;
	char *payload = (char*)malloc(payload_length);
	memcpy(payload, IPC_MAGIC, sizeof(IPC_MAGIC) - 1);
	uint32_t command = IPC_EVENT_SUBSCRIBE;
	memcpy(payload + sizeof(IPC_MAGIC) - 1, &command, sizeof(uint32_t));
	memcpy(payload + sizeof(IPC_MAGIC) - 1 + sizeof(uint32_t), &length, sizeof(uint32_t));
	memcpy(payload + sizeof(IPC_MAGIC) - 1 + sizeof(uint32_t) * 2, message, length);
	write(sockfd, payload, payload_length);
	::free(payload);

	pthread_t thread_id;
	if (pthread_create(&thread_id, NULL, GlobalHotkey::_sway_event_thread, this)) {
		// Could not create the thread
		this->_sway_deinit();
		return false;
	}
	this->_sway_event_thread_id = thread_id;

	// Ok!
	return true;
}

void* GlobalHotkey::_sway_event_thread(void* arg) {
	GlobalHotkey* self = (GlobalHotkey*)arg;

	int sockfd = self->_sway_socket;

	fd_set readfds;
	struct timeval timeout;

	while (1) {
		FD_ZERO(&readfds);
		FD_SET(sockfd, &readfds);
		timeout.tv_sec = 5;
		timeout.tv_usec = 0;

		int retval = select(sockfd + 1, &readfds, NULL, NULL, &timeout);
		if (retval == -1) {
			// Could not use select / connection dropped
			break;
		} else if (retval) {
			char buffer[4096];
			int nbytes = read(sockfd, buffer, sizeof(buffer));
			if (nbytes > 0) {
				//printf("Received event: %.*s\n", nbytes, buffer);
			}
		} else {
			// Timeout (Idle time)
		}
	}

	return NULL;
}

bool GlobalHotkey::_sway_deinit() {
	// Close the connection, if any
	if (this->_sway_socket) {
		close(this->_sway_socket);
		this->_sway_socket = 0;
	}

	// Wait for the event thread to end
	if (this->_sway_event_thread_id) {
		pthread_join(this->_sway_event_thread_id, NULL);
		this->_sway_event_thread_id = 0;
	}

	return true;
}

bool GlobalHotkey::_linux_deinit() {
	this->_sway_deinit();

	return false;
}

#endif
