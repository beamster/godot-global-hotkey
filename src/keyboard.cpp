/**************************************************************************/
/*  keyboard.cpp                                                          */
/**************************************************************************/
/*                         This file is part of:                          */
/*                             GODOT ENGINE                               */
/*                        https://godotengine.org                         */
/**************************************************************************/
/* Copyright (c) 2014-present Godot Engine contributors (see AUTHORS.md). */
/* Copyright (c) 2007-2014 Juan Linietsky, Ariel Manzur.                  */
/*                                                                        */
/* Permission is hereby granted, free of charge, to any person obtaining  */
/* a copy of this software and associated documentation files (the        */
/* "Software"), to deal in the Software without restriction, including    */
/* without limitation the rights to use, copy, modify, merge, publish,    */
/* distribute, sublicense, and/or sell copies of the Software, and to     */
/* permit persons to whom the Software is furnished to do so, subject to  */
/* the following conditions:                                              */
/*                                                                        */
/* The above copyright notice and this permission notice shall be         */
/* included in all copies or substantial portions of the Software.        */
/*                                                                        */
/* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,        */
/* EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF     */
/* MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. */
/* IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY   */
/* CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,   */
/* TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE      */
/* SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.                 */
/**************************************************************************/

// This is modified from core/os/keyboard.cpp from Godot
#include "keyboard.h"

using namespace godot;

struct _KeyCodeText {
	EngineKey code;
	const char *text;
};

static const _KeyCodeText _keycodes[] = {
	/* clang-format off */
	{EngineKey::ESCAPE                ,"Escape"},
	{EngineKey::TAB                   ,"Tab"},
	{EngineKey::BACKTAB               ,"Backtab"},
	{EngineKey::BACKSPACE             ,"Backspace"},
	{EngineKey::ENTER                 ,"Enter"},
	{EngineKey::KP_ENTER              ,"Kp Enter"},
	{EngineKey::INSERT                ,"Insert"},
	{EngineKey::KEY_DELETE            ,"Delete"},
	{EngineKey::PAUSE                 ,"Pause"},
	{EngineKey::PRINT                 ,"Print"},
	{EngineKey::SYSREQ                ,"SysReq"},
	{EngineKey::CLEAR                 ,"Clear"},
	{EngineKey::HOME                  ,"Home"},
	{EngineKey::END                   ,"End"},
	{EngineKey::LEFT                  ,"Left"},
	{EngineKey::UP                    ,"Up"},
	{EngineKey::RIGHT                 ,"Right"},
	{EngineKey::DOWN                  ,"Down"},
	{EngineKey::PAGEUP                ,"PageUp"},
	{EngineKey::PAGEDOWN              ,"PageDown"},
	{EngineKey::SHIFT                 ,"Shift"},
	{EngineKey::CTRL                  ,"Ctrl"},
#if defined(MACOS_ENABLED)
	{EngineKey::META                  ,"Command"},
	{EngineKey::CMD_OR_CTRL           ,"Command"},
	{EngineKey::ALT                   ,"Option"},
#elif defined(WINDOWS_ENABLED)
	{EngineKey::META                  ,"Windows"},
	{EngineKey::CMD_OR_CTRL           ,"Ctrl"},
	{EngineKey::ALT                   ,"Alt"},
#else
	{EngineKey::META                  ,"Meta"},
	{EngineKey::CMD_OR_CTRL           ,"Ctrl"},
	{EngineKey::ALT                   ,"Alt"},
#endif
	{EngineKey::CAPSLOCK              ,"CapsLock"},
	{EngineKey::NUMLOCK               ,"NumLock"},
	{EngineKey::SCROLLLOCK            ,"ScrollLock"},
	{EngineKey::F1                    ,"F1"},
	{EngineKey::F2                    ,"F2"},
	{EngineKey::F3                    ,"F3"},
	{EngineKey::F4                    ,"F4"},
	{EngineKey::F5                    ,"F5"},
	{EngineKey::F6                    ,"F6"},
	{EngineKey::F7                    ,"F7"},
	{EngineKey::F8                    ,"F8"},
	{EngineKey::F9                    ,"F9"},
	{EngineKey::F10                   ,"F10"},
	{EngineKey::F11                   ,"F11"},
	{EngineKey::F12                   ,"F12"},
	{EngineKey::F13                   ,"F13"},
	{EngineKey::F14                   ,"F14"},
	{EngineKey::F15                   ,"F15"},
	{EngineKey::F16                   ,"F16"},
	{EngineKey::F17                   ,"F17"},
	{EngineKey::F18                   ,"F18"},
	{EngineKey::F19                   ,"F19"},
	{EngineKey::F20                   ,"F20"},
	{EngineKey::F21                   ,"F21"},
	{EngineKey::F22                   ,"F22"},
	{EngineKey::F23                   ,"F23"},
	{EngineKey::F24                   ,"F24"},
	{EngineKey::F25                   ,"F25"},
	{EngineKey::F26                   ,"F26"},
	{EngineKey::F27                   ,"F27"},
	{EngineKey::F28                   ,"F28"},
	{EngineKey::F29                   ,"F29"},
	{EngineKey::F30                   ,"F30"},
	{EngineKey::F31                   ,"F31"},
	{EngineKey::F32                   ,"F32"},
	{EngineKey::F33                   ,"F33"},
	{EngineKey::F34                   ,"F34"},
	{EngineKey::F35                   ,"F35"},
	{EngineKey::KP_MULTIPLY           ,"Kp Multiply"},
	{EngineKey::KP_DIVIDE             ,"Kp Divide"},
	{EngineKey::KP_SUBTRACT           ,"Kp Subtract"},
	{EngineKey::KP_PERIOD             ,"Kp Period"},
	{EngineKey::KP_ADD                ,"Kp Add"},
	{EngineKey::KP_0                  ,"Kp 0"},
	{EngineKey::KP_1                  ,"Kp 1"},
	{EngineKey::KP_2                  ,"Kp 2"},
	{EngineKey::KP_3                  ,"Kp 3"},
	{EngineKey::KP_4                  ,"Kp 4"},
	{EngineKey::KP_5                  ,"Kp 5"},
	{EngineKey::KP_6                  ,"Kp 6"},
	{EngineKey::KP_7                  ,"Kp 7"},
	{EngineKey::KP_8                  ,"Kp 8"},
	{EngineKey::KP_9                  ,"Kp 9"},
	{EngineKey::MENU                  ,"Menu"},
	{EngineKey::HYPER                 ,"Hyper"},
	{EngineKey::HELP                  ,"Help"},
	{EngineKey::BACK                  ,"Back"},
	{EngineKey::FORWARD               ,"Forward"},
	{EngineKey::STOP                  ,"Stop"},
	{EngineKey::REFRESH               ,"Refresh"},
	{EngineKey::VOLUMEDOWN            ,"VolumeDown"},
	{EngineKey::VOLUMEMUTE            ,"VolumeMute"},
	{EngineKey::VOLUMEUP              ,"VolumeUp"},
	{EngineKey::MEDIAPLAY             ,"MediaPlay"},
	{EngineKey::MEDIASTOP             ,"MediaStop"},
	{EngineKey::MEDIAPREVIOUS         ,"MediaPrevious"},
	{EngineKey::MEDIANEXT             ,"MediaNext"},
	{EngineKey::MEDIARECORD           ,"MediaRecord"},
	{EngineKey::HOMEPAGE              ,"HomePage"},
	{EngineKey::FAVORITES             ,"Favorites"},
	{EngineKey::SEARCH                ,"Search"},
	{EngineKey::STANDBY               ,"StandBy"},
	{EngineKey::LAUNCHMAIL            ,"LaunchMail"},
	{EngineKey::LAUNCHMEDIA           ,"LaunchMedia"},
	{EngineKey::LAUNCH0               ,"Launch0"},
	{EngineKey::LAUNCH1               ,"Launch1"},
	{EngineKey::LAUNCH2               ,"Launch2"},
	{EngineKey::LAUNCH3               ,"Launch3"},
	{EngineKey::LAUNCH4               ,"Launch4"},
	{EngineKey::LAUNCH5               ,"Launch5"},
	{EngineKey::LAUNCH6               ,"Launch6"},
	{EngineKey::LAUNCH7               ,"Launch7"},
	{EngineKey::LAUNCH8               ,"Launch8"},
	{EngineKey::LAUNCH9               ,"Launch9"},
	{EngineKey::LAUNCHA               ,"LaunchA"},
	{EngineKey::LAUNCHB               ,"LaunchB"},
	{EngineKey::LAUNCHC               ,"LaunchC"},
	{EngineKey::LAUNCHD               ,"LaunchD"},
	{EngineKey::LAUNCHE               ,"LaunchE"},
	{EngineKey::LAUNCHF               ,"LaunchF"},
	{EngineKey::GLOBE                 ,"Globe"},
	{EngineKey::KEYBOARD              ,"On-screen keyboard"},
	{EngineKey::JIS_EISU              ,"JIS Eisu"},
	{EngineKey::JIS_KANA              ,"JIS Kana"},
	{EngineKey::UNKNOWN               ,"Unknown"},
	{EngineKey::SPACE                 ,"Space"},
	{EngineKey::EXCLAM                ,"Exclam"},
	{EngineKey::QUOTEDBL              ,"QuoteDbl"},
	{EngineKey::NUMBERSIGN            ,"NumberSign"},
	{EngineKey::DOLLAR                ,"Dollar"},
	{EngineKey::PERCENT               ,"Percent"},
	{EngineKey::AMPERSAND             ,"Ampersand"},
	{EngineKey::APOSTROPHE            ,"Apostrophe"},
	{EngineKey::PARENLEFT             ,"ParenLeft"},
	{EngineKey::PARENRIGHT            ,"ParenRight"},
	{EngineKey::ASTERISK              ,"Asterisk"},
	{EngineKey::PLUS                  ,"Plus"},
	{EngineKey::COMMA                 ,"Comma"},
	{EngineKey::MINUS                 ,"Minus"},
	{EngineKey::PERIOD                ,"Period"},
	{EngineKey::SLASH                 ,"Slash"},
	{EngineKey::KEY_0                 ,"0"},
	{EngineKey::KEY_1                 ,"1"},
	{EngineKey::KEY_2                 ,"2"},
	{EngineKey::KEY_3                 ,"3"},
	{EngineKey::KEY_4                 ,"4"},
	{EngineKey::KEY_5                 ,"5"},
	{EngineKey::KEY_6                 ,"6"},
	{EngineKey::KEY_7                 ,"7"},
	{EngineKey::KEY_8                 ,"8"},
	{EngineKey::KEY_9                 ,"9"},
	{EngineKey::COLON                 ,"Colon"},
	{EngineKey::SEMICOLON             ,"Semicolon"},
	{EngineKey::LESS                  ,"Less"},
	{EngineKey::EQUAL                 ,"Equal"},
	{EngineKey::GREATER               ,"Greater"},
	{EngineKey::QUESTION              ,"Question"},
	{EngineKey::AT                    ,"At"},
	{EngineKey::A                     ,"A"},
	{EngineKey::B                     ,"B"},
	{EngineKey::C                     ,"C"},
	{EngineKey::D                     ,"D"},
	{EngineKey::E                     ,"E"},
	{EngineKey::F                     ,"F"},
	{EngineKey::G                     ,"G"},
	{EngineKey::H                     ,"H"},
	{EngineKey::I                     ,"I"},
	{EngineKey::J                     ,"J"},
	{EngineKey::K                     ,"K"},
	{EngineKey::L                     ,"L"},
	{EngineKey::M                     ,"M"},
	{EngineKey::N                     ,"N"},
	{EngineKey::O                     ,"O"},
	{EngineKey::P                     ,"P"},
	{EngineKey::Q                     ,"Q"},
	{EngineKey::R                     ,"R"},
	{EngineKey::S                     ,"S"},
	{EngineKey::T                     ,"T"},
	{EngineKey::U                     ,"U"},
	{EngineKey::V                     ,"V"},
	{EngineKey::W                     ,"W"},
	{EngineKey::X                     ,"X"},
	{EngineKey::Y                     ,"Y"},
	{EngineKey::Z                     ,"Z"},
	{EngineKey::BRACKETLEFT           ,"BracketLeft"},
	{EngineKey::BACKSLASH             ,"BackSlash"},
	{EngineKey::BRACKETRIGHT          ,"BracketRight"},
	{EngineKey::ASCIICIRCUM           ,"AsciiCircum"},
	{EngineKey::UNDERSCORE            ,"UnderScore"},
	{EngineKey::QUOTELEFT             ,"QuoteLeft"},
	{EngineKey::BRACELEFT             ,"BraceLeft"},
	{EngineKey::BAR                   ,"Bar"},
	{EngineKey::BRACERIGHT            ,"BraceRight"},
	{EngineKey::ASCIITILDE            ,"AsciiTilde"},
	{EngineKey::NONE                  ,nullptr}
	/* clang-format on */
};

bool keycode_has_unicode(EngineKey p_keycode) {
	switch (p_keycode) {
		case EngineKey::ESCAPE:
		case EngineKey::TAB:
		case EngineKey::BACKTAB:
		case EngineKey::BACKSPACE:
		case EngineKey::ENTER:
		case EngineKey::KP_ENTER:
		case EngineKey::INSERT:
		case EngineKey::KEY_DELETE:
		case EngineKey::PAUSE:
		case EngineKey::PRINT:
		case EngineKey::SYSREQ:
		case EngineKey::CLEAR:
		case EngineKey::HOME:
		case EngineKey::END:
		case EngineKey::LEFT:
		case EngineKey::UP:
		case EngineKey::RIGHT:
		case EngineKey::DOWN:
		case EngineKey::PAGEUP:
		case EngineKey::PAGEDOWN:
		case EngineKey::SHIFT:
		case EngineKey::CTRL:
		case EngineKey::META:
		case EngineKey::ALT:
		case EngineKey::CAPSLOCK:
		case EngineKey::NUMLOCK:
		case EngineKey::SCROLLLOCK:
		case EngineKey::F1:
		case EngineKey::F2:
		case EngineKey::F3:
		case EngineKey::F4:
		case EngineKey::F5:
		case EngineKey::F6:
		case EngineKey::F7:
		case EngineKey::F8:
		case EngineKey::F9:
		case EngineKey::F10:
		case EngineKey::F11:
		case EngineKey::F12:
		case EngineKey::F13:
		case EngineKey::F14:
		case EngineKey::F15:
		case EngineKey::F16:
		case EngineKey::F17:
		case EngineKey::F18:
		case EngineKey::F19:
		case EngineKey::F20:
		case EngineKey::F21:
		case EngineKey::F22:
		case EngineKey::F23:
		case EngineKey::F24:
		case EngineKey::F25:
		case EngineKey::F26:
		case EngineKey::F27:
		case EngineKey::F28:
		case EngineKey::F29:
		case EngineKey::F30:
		case EngineKey::F31:
		case EngineKey::F32:
		case EngineKey::F33:
		case EngineKey::F34:
		case EngineKey::F35:
		case EngineKey::MENU:
		case EngineKey::HYPER:
		case EngineKey::HELP:
		case EngineKey::BACK:
		case EngineKey::FORWARD:
		case EngineKey::STOP:
		case EngineKey::REFRESH:
		case EngineKey::VOLUMEDOWN:
		case EngineKey::VOLUMEMUTE:
		case EngineKey::VOLUMEUP:
		case EngineKey::MEDIAPLAY:
		case EngineKey::MEDIASTOP:
		case EngineKey::MEDIAPREVIOUS:
		case EngineKey::MEDIANEXT:
		case EngineKey::MEDIARECORD:
		case EngineKey::HOMEPAGE:
		case EngineKey::FAVORITES:
		case EngineKey::SEARCH:
		case EngineKey::STANDBY:
		case EngineKey::OPENURL:
		case EngineKey::LAUNCHMAIL:
		case EngineKey::LAUNCHMEDIA:
		case EngineKey::LAUNCH0:
		case EngineKey::LAUNCH1:
		case EngineKey::LAUNCH2:
		case EngineKey::LAUNCH3:
		case EngineKey::LAUNCH4:
		case EngineKey::LAUNCH5:
		case EngineKey::LAUNCH6:
		case EngineKey::LAUNCH7:
		case EngineKey::LAUNCH8:
		case EngineKey::LAUNCH9:
		case EngineKey::LAUNCHA:
		case EngineKey::LAUNCHB:
		case EngineKey::LAUNCHC:
		case EngineKey::LAUNCHD:
		case EngineKey::LAUNCHE:
		case EngineKey::LAUNCHF:
		case EngineKey::GLOBE:
		case EngineKey::KEYBOARD:
		case EngineKey::JIS_EISU:
		case EngineKey::JIS_KANA:
			return false;
		default: {
		}
	}

	return true;
}

String keycode_get_string(EngineKey p_code) {
	String codestr;
	if ((p_code & EngineKeyModifierMask::SHIFT) != EngineKey::NONE) {
		codestr += find_keycode_name(EngineKey::SHIFT);
		codestr += "+";
	}
	if ((p_code & EngineKeyModifierMask::ALT) != EngineKey::NONE) {
		codestr += find_keycode_name(EngineKey::ALT);
		codestr += "+";
	}
	if ((p_code & EngineKeyModifierMask::CMD_OR_CTRL) != EngineKey::NONE) {
#ifdef _OSX_
		//if (OS::get_singleton()->has_feature("macos") || OS::get_singleton()->has_feature("web_macos") || OS::get_singleton()->has_feature("web_ios")) {
			codestr += find_keycode_name(EngineKey::META);
#else
		//} else {
			codestr += find_keycode_name(EngineKey::CTRL);
		//}
#endif
		codestr += "+";
	}
	if ((p_code & EngineKeyModifierMask::CTRL) != EngineKey::NONE) {
		codestr += find_keycode_name(EngineKey::CTRL);
		codestr += "+";
	}
	if ((p_code & EngineKeyModifierMask::META) != EngineKey::NONE) {
		codestr += find_keycode_name(EngineKey::META);
		codestr += "+";
	}

	p_code &= EngineKeyModifierMask::CODE_MASK;

	const _KeyCodeText *kct = &_keycodes[0];

	while (kct->text) {
		if (kct->code == p_code) {
			codestr += kct->text;
			return codestr;
		}
		kct++;
	}

	codestr += String::chr((char32_t)p_code);

	return codestr;
}

EngineKey find_keycode(const String &p_codestr) {
	EngineKey keycode = EngineKey::NONE;
	godot::PackedStringArray code_parts = p_codestr.split("+");
	if (code_parts.size() < 1) {
		return keycode;
	}

	String last_part = code_parts[code_parts.size() - 1];
	const _KeyCodeText *kct = &_keycodes[0];

	while (kct->text) {
		if (last_part.nocasecmp_to(kct->text) == 0) {
			keycode = kct->code;
			break;
		}
		kct++;
	}

	for (int part = 0; part < code_parts.size() - 1; part++) {
		String code_part = code_parts[part];
		if (code_part.nocasecmp_to(find_keycode_name(EngineKey::SHIFT)) == 0) {
			keycode |= EngineKeyModifierMask::SHIFT;
		} else if (code_part.nocasecmp_to(find_keycode_name(EngineKey::CTRL)) == 0) {
			keycode |= EngineKeyModifierMask::CTRL;
		} else if (code_part.nocasecmp_to(find_keycode_name(EngineKey::META)) == 0) {
			keycode |= EngineKeyModifierMask::META;
		} else if (code_part.nocasecmp_to(find_keycode_name(EngineKey::ALT)) == 0) {
			keycode |= EngineKeyModifierMask::ALT;
		}
	}

	return keycode;
}

const char *find_keycode_name(EngineKey p_keycode) {
	const _KeyCodeText *kct = &_keycodes[0];

	while (kct->text) {
		if (kct->code == p_keycode) {
			return kct->text;
		}
		kct++;
	}

	return "";
}

int keycode_get_count() {
	const _KeyCodeText *kct = &_keycodes[0];

	int count = 0;
	while (kct->text) {
		count++;
		kct++;
	}
	return count;
}

int keycode_get_value_by_index(int p_index) {
	return (int)_keycodes[p_index].code;
}

const char *keycode_get_name_by_index(int p_index) {
	return _keycodes[p_index].text;
}

char32_t fix_unicode(char32_t p_char) {
	if (p_char >= 0x20 && p_char != 0x7F) {
		return p_char;
	}
	return 0;
}

/*
EngineKey fix_keycode(char32_t p_char, EngineKey p_key) {
	if (p_char >= 0x20 && p_char <= 0x7E) {
		return (EngineKey)String::char_uppercase(p_char);
	}
	return p_key;
}

EngineKey fix_key_label(char32_t p_char, EngineKey p_key) {
	if (p_char >= 0x20 && p_char != 0x7F) {
		return (EngineKey)String::char_uppercase(p_char);
	}
	return p_key;
}
*/
