#ifndef GLOBAL_HOTKEY_HPP
#define GLOBAL_HOTKEY_HPP

// Import standard library functions

// Import platform specific libraries
#ifdef _WIN32
#include <windows.h>
#include "platform/windows/key_mapping_windows.h"
#else
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <unistd.h>

// Threads and select for some blocking requests
#include <pthread.h>
#include <sys/select.h>

#include "platform/linux/wayland/key_mapping_xkb.h"
#include "platform/linux/x11/key_mapping_x11.h"

// Sway magic strings
#define IPC_MAGIC "i3-ipc"
#define IPC_EVENT_SUBSCRIBE 2
#define IPC_EVENT_KEYBOARD 19
#endif

// Import Godot support classes
#include <godot_cpp/core/class_db.hpp>

// Import base class
#include <godot_cpp/classes/object.hpp>

namespace godot {
	/**
	 * This class provides a callback mechanism for global keypresses.
	 */
	class GlobalHotkey : public Object {
		GDCLASS(GlobalHotkey, Object)

		private:
			// Platform specific
#ifdef _WIN32
			bool _win_init();
			bool _win_deinit();
			static LRESULT KeyboardProc(int nCode, WPARAM wParam, LPARAM lParam);
#else
			bool _linux_init();
			bool _sway_init(const char* socket_path);
			bool _sway_deinit();
			pthread_t _sway_event_thread_id = 0;
			static void* _sway_event_thread(void* arg);
			bool _linux_deinit();
			int _sway_socket = 0;
#endif

			bool _supported = false;

		public:
			// Constructor / Destructor
			GlobalHotkey();
			~GlobalHotkey();

			bool is_supported();

		protected:
			// Godot bind call
			static void _bind_methods();

			// When a keypress is known
			void _emit(
				int keycode,
				int physical_keycode,
			       	bool pressed,
				bool shift_pressed,
				bool alt_pressed,
				bool control_pressed,
				bool meta_pressed
			);
	};
}
#endif
