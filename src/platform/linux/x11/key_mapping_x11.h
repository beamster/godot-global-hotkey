/**************************************************************************/
/*  key_mapping_x11.h                                                     */
/**************************************************************************/
/*                         This file is part of:                          */
/*                             GODOT ENGINE                               */
/*                        https://godotengine.org                         */
/**************************************************************************/
/* Copyright (c) 2014-present Godot Engine contributors (see AUTHORS.md). */
/* Copyright (c) 2007-2014 Juan Linietsky, Ariel Manzur.                  */
/*                                                                        */
/* Permission is hereby granted, free of charge, to any person obtaining  */
/* a copy of this software and associated documentation files (the        */
/* "Software"), to deal in the Software without restriction, including    */
/* without limitation the rights to use, copy, modify, merge, publish,    */
/* distribute, sublicense, and/or sell copies of the Software, and to     */
/* permit persons to whom the Software is furnished to do so, subject to  */
/* the following conditions:                                              */
/*                                                                        */
/* The above copyright notice and this permission notice shall be         */
/* included in all copies or substantial portions of the Software.        */
/*                                                                        */
/* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,        */
/* EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF     */
/* MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. */
/* IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY   */
/* CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,   */
/* TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE      */
/* SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.                 */
/**************************************************************************/

#ifndef KEY_MAPPING_X11_H
#define KEY_MAPPING_X11_H

#include <godot_cpp/core/class_db.hpp>
#include <godot_cpp/classes/object.hpp>
#include <godot_cpp/templates/hash_map.hpp>
#include "keyboard.h"

#include <X11/XF86keysym.h>
#include <X11/Xlib.h>

#define XK_MISCELLANY
#define XK_LATIN1
#define XK_XKB_KEYS
#include <X11/keysymdef.h>

// Adding:
using namespace godot;

class KeyMappingX11 {
	struct HashMapHasherKeys {
		static _FORCE_INLINE_ uint32_t hash(const EngineKey p_key) { return hash_fmix32(static_cast<uint32_t>(p_key)); }
		static _FORCE_INLINE_ uint32_t hash(const char32_t p_uchar) { return hash_fmix32(p_uchar); }
		static _FORCE_INLINE_ uint32_t hash(const unsigned p_key) { return hash_fmix32(p_key); }
		static _FORCE_INLINE_ uint32_t hash(const KeySym p_key) { return hash_fmix32(p_key); }
	};

	static inline HashMap<KeySym, EngineKey, HashMapHasherKeys> xkeysym_map;
	static inline HashMap<unsigned int, EngineKey, HashMapHasherKeys> scancode_map;
	static inline HashMap<EngineKey, unsigned int, HashMapHasherKeys> scancode_map_inv;
	static inline HashMap<KeySym, char32_t, HashMapHasherKeys> xkeysym_unicode_map;
	static inline HashMap<unsigned int, KeyLocation, HashMapHasherKeys> location_map;

	KeyMappingX11() {}

public:
	static void initialize();

	static EngineKey get_keycode(KeySym p_keysym);
	static unsigned int get_xlibcode(EngineKey p_keysym);
	static EngineKey get_scancode(unsigned int p_code);
	static char32_t get_unicode_from_keysym(KeySym p_keysym);
	static KeyLocation get_location(unsigned int p_code);
};

#endif // KEY_MAPPING_X11_H
