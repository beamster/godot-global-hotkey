/**************************************************************************/
/*  key_mapping_xkb.cpp                                                   */
/**************************************************************************/
/*                         This file is part of:                          */
/*                             GODOT ENGINE                               */
/*                        https://godotengine.org                         */
/**************************************************************************/
/* Copyright (c) 2014-present Godot Engine contributors (see AUTHORS.md). */
/* Copyright (c) 2007-2014 Juan Linietsky, Ariel Manzur.                  */
/*                                                                        */
/* Permission is hereby granted, free of charge, to any person obtaining  */
/* a copy of this software and associated documentation files (the        */
/* "Software"), to deal in the Software without restriction, including    */
/* without limitation the rights to use, copy, modify, merge, publish,    */
/* distribute, sublicense, and/or sell copies of the Software, and to     */
/* permit persons to whom the Software is furnished to do so, subject to  */
/* the following conditions:                                              */
/*                                                                        */
/* The above copyright notice and this permission notice shall be         */
/* included in all copies or substantial portions of the Software.        */
/*                                                                        */
/* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,        */
/* EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF     */
/* MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. */
/* IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY   */
/* CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,   */
/* TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE      */
/* SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.                 */
/**************************************************************************/

#include "key_mapping_xkb.h"

void KeyMappingXKB::initialize() {
	// XKB keycode to Godot Key map.

	xkb_keycode_map[XKB_KEY_Escape] = EngineKey::ESCAPE;
	xkb_keycode_map[XKB_KEY_Tab] = EngineKey::TAB;
	xkb_keycode_map[XKB_KEY_ISO_Left_Tab] = EngineKey::BACKTAB;
	xkb_keycode_map[XKB_KEY_BackSpace] = EngineKey::BACKSPACE;
	xkb_keycode_map[XKB_KEY_Return] = EngineKey::ENTER;
	xkb_keycode_map[XKB_KEY_Insert] = EngineKey::INSERT;
	xkb_keycode_map[XKB_KEY_Delete] = EngineKey::KEY_DELETE;
	xkb_keycode_map[XKB_KEY_Clear] = EngineKey::KEY_DELETE;
	xkb_keycode_map[XKB_KEY_Pause] = EngineKey::PAUSE;
	xkb_keycode_map[XKB_KEY_Print] = EngineKey::PRINT;
	xkb_keycode_map[XKB_KEY_Home] = EngineKey::HOME;
	xkb_keycode_map[XKB_KEY_End] = EngineKey::END;
	xkb_keycode_map[XKB_KEY_Left] = EngineKey::LEFT;
	xkb_keycode_map[XKB_KEY_Up] = EngineKey::UP;
	xkb_keycode_map[XKB_KEY_Right] = EngineKey::RIGHT;
	xkb_keycode_map[XKB_KEY_Down] = EngineKey::DOWN;
	xkb_keycode_map[XKB_KEY_Prior] = EngineKey::PAGEUP;
	xkb_keycode_map[XKB_KEY_Next] = EngineKey::PAGEDOWN;
	xkb_keycode_map[XKB_KEY_Shift_L] = EngineKey::SHIFT;
	xkb_keycode_map[XKB_KEY_Shift_R] = EngineKey::SHIFT;
	xkb_keycode_map[XKB_KEY_Shift_Lock] = EngineKey::SHIFT;
	xkb_keycode_map[XKB_KEY_Control_L] = EngineKey::CTRL;
	xkb_keycode_map[XKB_KEY_Control_R] = EngineKey::CTRL;
	xkb_keycode_map[XKB_KEY_Meta_L] = EngineKey::META;
	xkb_keycode_map[XKB_KEY_Meta_R] = EngineKey::META;
	xkb_keycode_map[XKB_KEY_Alt_L] = EngineKey::ALT;
	xkb_keycode_map[XKB_KEY_Alt_R] = EngineKey::ALT;
	xkb_keycode_map[XKB_KEY_Caps_Lock] = EngineKey::CAPSLOCK;
	xkb_keycode_map[XKB_KEY_Num_Lock] = EngineKey::NUMLOCK;
	xkb_keycode_map[XKB_KEY_Scroll_Lock] = EngineKey::SCROLLLOCK;
	xkb_keycode_map[XKB_KEY_less] = EngineKey::QUOTELEFT;
	xkb_keycode_map[XKB_KEY_grave] = EngineKey::SECTION;
	xkb_keycode_map[XKB_KEY_Super_L] = EngineKey::META;
	xkb_keycode_map[XKB_KEY_Super_R] = EngineKey::META;
	xkb_keycode_map[XKB_KEY_Menu] = EngineKey::MENU;
	xkb_keycode_map[XKB_KEY_Hyper_L] = EngineKey::HYPER;
	xkb_keycode_map[XKB_KEY_Hyper_R] = EngineKey::HYPER;
	xkb_keycode_map[XKB_KEY_Help] = EngineKey::HELP;
	xkb_keycode_map[XKB_KEY_KP_Space] = EngineKey::SPACE;
	xkb_keycode_map[XKB_KEY_KP_Tab] = EngineKey::TAB;
	xkb_keycode_map[XKB_KEY_KP_Enter] = EngineKey::KP_ENTER;
	xkb_keycode_map[XKB_KEY_Home] = EngineKey::HOME;
	xkb_keycode_map[XKB_KEY_Left] = EngineKey::LEFT;
	xkb_keycode_map[XKB_KEY_Up] = EngineKey::UP;
	xkb_keycode_map[XKB_KEY_Right] = EngineKey::RIGHT;
	xkb_keycode_map[XKB_KEY_Down] = EngineKey::DOWN;
	xkb_keycode_map[XKB_KEY_Prior] = EngineKey::PAGEUP;
	xkb_keycode_map[XKB_KEY_Next] = EngineKey::PAGEDOWN;
	xkb_keycode_map[XKB_KEY_End] = EngineKey::END;
	xkb_keycode_map[XKB_KEY_Begin] = EngineKey::CLEAR;
	xkb_keycode_map[XKB_KEY_Insert] = EngineKey::INSERT;
	xkb_keycode_map[XKB_KEY_Delete] = EngineKey::KEY_DELETE;
	xkb_keycode_map[XKB_KEY_KP_Equal] = EngineKey::EQUAL;
	xkb_keycode_map[XKB_KEY_KP_Separator] = EngineKey::COMMA;
	xkb_keycode_map[XKB_KEY_KP_Decimal] = EngineKey::KP_PERIOD;
	xkb_keycode_map[XKB_KEY_KP_Multiply] = EngineKey::KP_MULTIPLY;
	xkb_keycode_map[XKB_KEY_KP_Divide] = EngineKey::KP_DIVIDE;
	xkb_keycode_map[XKB_KEY_KP_Subtract] = EngineKey::KP_SUBTRACT;
	xkb_keycode_map[XKB_KEY_KP_Add] = EngineKey::KP_ADD;
	xkb_keycode_map[XKB_KEY_KP_0] = EngineKey::KP_0;
	xkb_keycode_map[XKB_KEY_KP_1] = EngineKey::KP_1;
	xkb_keycode_map[XKB_KEY_KP_2] = EngineKey::KP_2;
	xkb_keycode_map[XKB_KEY_KP_3] = EngineKey::KP_3;
	xkb_keycode_map[XKB_KEY_KP_4] = EngineKey::KP_4;
	xkb_keycode_map[XKB_KEY_KP_5] = EngineKey::KP_5;
	xkb_keycode_map[XKB_KEY_KP_6] = EngineKey::KP_6;
	xkb_keycode_map[XKB_KEY_KP_7] = EngineKey::KP_7;
	xkb_keycode_map[XKB_KEY_KP_8] = EngineKey::KP_8;
	xkb_keycode_map[XKB_KEY_KP_9] = EngineKey::KP_9;
	// Same keys but with numlock off.
	xkb_keycode_map[XKB_KEY_KP_Insert] = EngineKey::INSERT;
	xkb_keycode_map[XKB_KEY_KP_Delete] = EngineKey::KEY_DELETE;
	xkb_keycode_map[XKB_KEY_KP_End] = EngineKey::END;
	xkb_keycode_map[XKB_KEY_KP_Down] = EngineKey::DOWN;
	xkb_keycode_map[XKB_KEY_KP_Page_Down] = EngineKey::PAGEDOWN;
	xkb_keycode_map[XKB_KEY_KP_Left] = EngineKey::LEFT;
	// X11 documents this (numpad 5) as "begin of line" but no toolkit seems to interpret it this way.
	// On Windows this is emitting EngineKey::Clear so for consistency it will be mapped to EngineKey::Clear
	xkb_keycode_map[XKB_KEY_KP_Begin] = EngineKey::CLEAR;
	xkb_keycode_map[XKB_KEY_KP_Right] = EngineKey::RIGHT;
	xkb_keycode_map[XKB_KEY_KP_Home] = EngineKey::HOME;
	xkb_keycode_map[XKB_KEY_KP_Up] = EngineKey::UP;
	xkb_keycode_map[XKB_KEY_KP_Page_Up] = EngineKey::PAGEUP;
	xkb_keycode_map[XKB_KEY_F1] = EngineKey::F1;
	xkb_keycode_map[XKB_KEY_F2] = EngineKey::F2;
	xkb_keycode_map[XKB_KEY_F3] = EngineKey::F3;
	xkb_keycode_map[XKB_KEY_F4] = EngineKey::F4;
	xkb_keycode_map[XKB_KEY_F5] = EngineKey::F5;
	xkb_keycode_map[XKB_KEY_F6] = EngineKey::F6;
	xkb_keycode_map[XKB_KEY_F7] = EngineKey::F7;
	xkb_keycode_map[XKB_KEY_F8] = EngineKey::F8;
	xkb_keycode_map[XKB_KEY_F9] = EngineKey::F9;
	xkb_keycode_map[XKB_KEY_F10] = EngineKey::F10;
	xkb_keycode_map[XKB_KEY_F11] = EngineKey::F11;
	xkb_keycode_map[XKB_KEY_F12] = EngineKey::F12;
	xkb_keycode_map[XKB_KEY_F13] = EngineKey::F13;
	xkb_keycode_map[XKB_KEY_F14] = EngineKey::F14;
	xkb_keycode_map[XKB_KEY_F15] = EngineKey::F15;
	xkb_keycode_map[XKB_KEY_F16] = EngineKey::F16;
	xkb_keycode_map[XKB_KEY_F17] = EngineKey::F17;
	xkb_keycode_map[XKB_KEY_F18] = EngineKey::F18;
	xkb_keycode_map[XKB_KEY_F19] = EngineKey::F19;
	xkb_keycode_map[XKB_KEY_F20] = EngineKey::F20;
	xkb_keycode_map[XKB_KEY_F21] = EngineKey::F21;
	xkb_keycode_map[XKB_KEY_F22] = EngineKey::F22;
	xkb_keycode_map[XKB_KEY_F23] = EngineKey::F23;
	xkb_keycode_map[XKB_KEY_F24] = EngineKey::F24;
	xkb_keycode_map[XKB_KEY_F25] = EngineKey::F25;
	xkb_keycode_map[XKB_KEY_F26] = EngineKey::F26;
	xkb_keycode_map[XKB_KEY_F27] = EngineKey::F27;
	xkb_keycode_map[XKB_KEY_F28] = EngineKey::F28;
	xkb_keycode_map[XKB_KEY_F29] = EngineKey::F29;
	xkb_keycode_map[XKB_KEY_F30] = EngineKey::F30;
	xkb_keycode_map[XKB_KEY_F31] = EngineKey::F31;
	xkb_keycode_map[XKB_KEY_F32] = EngineKey::F32;
	xkb_keycode_map[XKB_KEY_F33] = EngineKey::F33;
	xkb_keycode_map[XKB_KEY_F34] = EngineKey::F34;
	xkb_keycode_map[XKB_KEY_F35] = EngineKey::F35;
	xkb_keycode_map[XKB_KEY_yen] = EngineKey::YEN;
	xkb_keycode_map[XKB_KEY_section] = EngineKey::SECTION;
	// Media keys.
	xkb_keycode_map[XKB_KEY_XF86Back] = EngineKey::BACK;
	xkb_keycode_map[XKB_KEY_XF86Forward] = EngineKey::FORWARD;
	xkb_keycode_map[XKB_KEY_XF86Stop] = EngineKey::STOP;
	xkb_keycode_map[XKB_KEY_XF86Refresh] = EngineKey::REFRESH;
	xkb_keycode_map[XKB_KEY_XF86Favorites] = EngineKey::FAVORITES;
	xkb_keycode_map[XKB_KEY_XF86OpenURL] = EngineKey::OPENURL;
	xkb_keycode_map[XKB_KEY_XF86HomePage] = EngineKey::HOMEPAGE;
	xkb_keycode_map[XKB_KEY_XF86Search] = EngineKey::SEARCH;
	xkb_keycode_map[XKB_KEY_XF86AudioLowerVolume] = EngineKey::VOLUMEDOWN;
	xkb_keycode_map[XKB_KEY_XF86AudioMute] = EngineKey::VOLUMEMUTE;
	xkb_keycode_map[XKB_KEY_XF86AudioRaiseVolume] = EngineKey::VOLUMEUP;
	xkb_keycode_map[XKB_KEY_XF86AudioPlay] = EngineKey::MEDIAPLAY;
	xkb_keycode_map[XKB_KEY_XF86AudioStop] = EngineKey::MEDIASTOP;
	xkb_keycode_map[XKB_KEY_XF86AudioPrev] = EngineKey::MEDIAPREVIOUS;
	xkb_keycode_map[XKB_KEY_XF86AudioNext] = EngineKey::MEDIANEXT;
	xkb_keycode_map[XKB_KEY_XF86AudioRecord] = EngineKey::MEDIARECORD;
	xkb_keycode_map[XKB_KEY_XF86Standby] = EngineKey::STANDBY;
	// Launch keys.
	xkb_keycode_map[XKB_KEY_XF86Mail] = EngineKey::LAUNCHMAIL;
	xkb_keycode_map[XKB_KEY_XF86AudioMedia] = EngineKey::LAUNCHMEDIA;
	xkb_keycode_map[XKB_KEY_XF86MyComputer] = EngineKey::LAUNCH0;
	xkb_keycode_map[XKB_KEY_XF86Calculator] = EngineKey::LAUNCH1;
	xkb_keycode_map[XKB_KEY_XF86Launch0] = EngineKey::LAUNCH2;
	xkb_keycode_map[XKB_KEY_XF86Launch1] = EngineKey::LAUNCH3;
	xkb_keycode_map[XKB_KEY_XF86Launch2] = EngineKey::LAUNCH4;
	xkb_keycode_map[XKB_KEY_XF86Launch3] = EngineKey::LAUNCH5;
	xkb_keycode_map[XKB_KEY_XF86Launch4] = EngineKey::LAUNCH6;
	xkb_keycode_map[XKB_KEY_XF86Launch5] = EngineKey::LAUNCH7;
	xkb_keycode_map[XKB_KEY_XF86Launch6] = EngineKey::LAUNCH8;
	xkb_keycode_map[XKB_KEY_XF86Launch7] = EngineKey::LAUNCH9;
	xkb_keycode_map[XKB_KEY_XF86Launch8] = EngineKey::LAUNCHA;
	xkb_keycode_map[XKB_KEY_XF86Launch9] = EngineKey::LAUNCHB;
	xkb_keycode_map[XKB_KEY_XF86LaunchA] = EngineKey::LAUNCHC;
	xkb_keycode_map[XKB_KEY_XF86LaunchB] = EngineKey::LAUNCHD;
	xkb_keycode_map[XKB_KEY_XF86LaunchC] = EngineKey::LAUNCHE;
	xkb_keycode_map[XKB_KEY_XF86LaunchD] = EngineKey::LAUNCHF;

	// Scancode to Godot Key map.
	scancode_map[0x09] = EngineKey::ESCAPE;
	scancode_map[0x0A] = EngineKey::KEY_1;
	scancode_map[0x0B] = EngineKey::KEY_2;
	scancode_map[0x0C] = EngineKey::KEY_3;
	scancode_map[0x0D] = EngineKey::KEY_4;
	scancode_map[0x0E] = EngineKey::KEY_5;
	scancode_map[0x0F] = EngineKey::KEY_6;
	scancode_map[0x10] = EngineKey::KEY_7;
	scancode_map[0x11] = EngineKey::KEY_8;
	scancode_map[0x12] = EngineKey::KEY_9;
	scancode_map[0x13] = EngineKey::KEY_0;
	scancode_map[0x14] = EngineKey::MINUS;
	scancode_map[0x15] = EngineKey::EQUAL;
	scancode_map[0x16] = EngineKey::BACKSPACE;
	scancode_map[0x17] = EngineKey::TAB;
	scancode_map[0x18] = EngineKey::Q;
	scancode_map[0x19] = EngineKey::W;
	scancode_map[0x1A] = EngineKey::E;
	scancode_map[0x1B] = EngineKey::R;
	scancode_map[0x1C] = EngineKey::T;
	scancode_map[0x1D] = EngineKey::Y;
	scancode_map[0x1E] = EngineKey::U;
	scancode_map[0x1F] = EngineKey::I;
	scancode_map[0x20] = EngineKey::O;
	scancode_map[0x21] = EngineKey::P;
	scancode_map[0x22] = EngineKey::BRACELEFT;
	scancode_map[0x23] = EngineKey::BRACERIGHT;
	scancode_map[0x24] = EngineKey::ENTER;
	scancode_map[0x25] = EngineKey::CTRL; // Left
	scancode_map[0x26] = EngineKey::A;
	scancode_map[0x27] = EngineKey::S;
	scancode_map[0x28] = EngineKey::D;
	scancode_map[0x29] = EngineKey::F;
	scancode_map[0x2A] = EngineKey::G;
	scancode_map[0x2B] = EngineKey::H;
	scancode_map[0x2C] = EngineKey::J;
	scancode_map[0x2D] = EngineKey::K;
	scancode_map[0x2E] = EngineKey::L;
	scancode_map[0x2F] = EngineKey::SEMICOLON;
	scancode_map[0x30] = EngineKey::APOSTROPHE;
	scancode_map[0x31] = EngineKey::SECTION;
	scancode_map[0x32] = EngineKey::SHIFT; // Left
	scancode_map[0x33] = EngineKey::BACKSLASH;
	scancode_map[0x34] = EngineKey::Z;
	scancode_map[0x35] = EngineKey::X;
	scancode_map[0x36] = EngineKey::C;
	scancode_map[0x37] = EngineKey::V;
	scancode_map[0x38] = EngineKey::B;
	scancode_map[0x39] = EngineKey::N;
	scancode_map[0x3A] = EngineKey::M;
	scancode_map[0x3B] = EngineKey::COMMA;
	scancode_map[0x3C] = EngineKey::PERIOD;
	scancode_map[0x3D] = EngineKey::SLASH;
	scancode_map[0x3E] = EngineKey::SHIFT; // Right
	scancode_map[0x3F] = EngineKey::KP_MULTIPLY;
	scancode_map[0x40] = EngineKey::ALT; // Left
	scancode_map[0x41] = EngineKey::SPACE;
	scancode_map[0x42] = EngineKey::CAPSLOCK;
	scancode_map[0x43] = EngineKey::F1;
	scancode_map[0x44] = EngineKey::F2;
	scancode_map[0x45] = EngineKey::F3;
	scancode_map[0x46] = EngineKey::F4;
	scancode_map[0x47] = EngineKey::F5;
	scancode_map[0x48] = EngineKey::F6;
	scancode_map[0x49] = EngineKey::F7;
	scancode_map[0x4A] = EngineKey::F8;
	scancode_map[0x4B] = EngineKey::F9;
	scancode_map[0x4C] = EngineKey::F10;
	scancode_map[0x4D] = EngineKey::NUMLOCK;
	scancode_map[0x4E] = EngineKey::SCROLLLOCK;
	scancode_map[0x4F] = EngineKey::KP_7;
	scancode_map[0x50] = EngineKey::KP_8;
	scancode_map[0x51] = EngineKey::KP_9;
	scancode_map[0x52] = EngineKey::KP_SUBTRACT;
	scancode_map[0x53] = EngineKey::KP_4;
	scancode_map[0x54] = EngineKey::KP_5;
	scancode_map[0x55] = EngineKey::KP_6;
	scancode_map[0x56] = EngineKey::KP_ADD;
	scancode_map[0x57] = EngineKey::KP_1;
	scancode_map[0x58] = EngineKey::KP_2;
	scancode_map[0x59] = EngineKey::KP_3;
	scancode_map[0x5A] = EngineKey::KP_0;
	scancode_map[0x5B] = EngineKey::KP_PERIOD;
	//scancode_map[0x5C]
	//scancode_map[0x5D] // Zenkaku Hankaku
	scancode_map[0x5E] = EngineKey::QUOTELEFT;
	scancode_map[0x5F] = EngineKey::F11;
	scancode_map[0x60] = EngineKey::F12;
	//scancode_map[0x61] // Romaji
	//scancode_map[0x62] // Katakana
	//scancode_map[0x63] // Hiragana
	//scancode_map[0x64] // Henkan
	//scancode_map[0x65] // Hiragana Katakana
	//scancode_map[0x66] // Muhenkan
	scancode_map[0x67] = EngineKey::COMMA; // KP_Separator
	scancode_map[0x68] = EngineKey::KP_ENTER;
	scancode_map[0x69] = EngineKey::CTRL; // Right
	scancode_map[0x6A] = EngineKey::KP_DIVIDE;
	scancode_map[0x6B] = EngineKey::PRINT;
	scancode_map[0x6C] = EngineKey::ALT; // Right
	scancode_map[0x6D] = EngineKey::ENTER;
	scancode_map[0x6E] = EngineKey::HOME;
	scancode_map[0x6F] = EngineKey::UP;
	scancode_map[0x70] = EngineKey::PAGEUP;
	scancode_map[0x71] = EngineKey::LEFT;
	scancode_map[0x72] = EngineKey::RIGHT;
	scancode_map[0x73] = EngineKey::END;
	scancode_map[0x74] = EngineKey::DOWN;
	scancode_map[0x75] = EngineKey::PAGEDOWN;
	scancode_map[0x76] = EngineKey::INSERT;
	scancode_map[0x77] = EngineKey::KEY_DELETE;
	//scancode_map[0x78] // Macro
	scancode_map[0x79] = EngineKey::VOLUMEMUTE;
	scancode_map[0x7A] = EngineKey::VOLUMEDOWN;
	scancode_map[0x7B] = EngineKey::VOLUMEUP;
	//scancode_map[0x7C] // Power
	scancode_map[0x7D] = EngineKey::EQUAL; // KP_Equal
	//scancode_map[0x7E] // KP_PlusMinus
	scancode_map[0x7F] = EngineKey::PAUSE;
	scancode_map[0x80] = EngineKey::LAUNCH0;
	scancode_map[0x81] = EngineKey::COMMA; // KP_Comma
	//scancode_map[0x82] // Hangul
	//scancode_map[0x83] // Hangul_Hanja
	scancode_map[0x84] = EngineKey::YEN;
	scancode_map[0x85] = EngineKey::META; // Left
	scancode_map[0x86] = EngineKey::META; // Right
	scancode_map[0x87] = EngineKey::MENU;

	scancode_map[0xA6] = EngineKey::BACK; // On Chromebooks
	scancode_map[0xA7] = EngineKey::FORWARD; // On Chromebooks

	scancode_map[0xB5] = EngineKey::REFRESH; // On Chromebooks

	scancode_map[0xBF] = EngineKey::F13;
	scancode_map[0xC0] = EngineKey::F14;
	scancode_map[0xC1] = EngineKey::F15;
	scancode_map[0xC2] = EngineKey::F16;
	scancode_map[0xC3] = EngineKey::F17;
	scancode_map[0xC4] = EngineKey::F18;
	scancode_map[0xC5] = EngineKey::F19;
	scancode_map[0xC6] = EngineKey::F20;
	scancode_map[0xC7] = EngineKey::F21;
	scancode_map[0xC8] = EngineKey::F22;
	scancode_map[0xC9] = EngineKey::F23;
	scancode_map[0xCA] = EngineKey::F24;
	scancode_map[0xCB] = EngineKey::F25;
	scancode_map[0xCC] = EngineKey::F26;
	scancode_map[0xCD] = EngineKey::F27;
	scancode_map[0xCE] = EngineKey::F28;
	scancode_map[0xCF] = EngineKey::F29;
	scancode_map[0xD0] = EngineKey::F30;
	scancode_map[0xD1] = EngineKey::F31;
	scancode_map[0xD2] = EngineKey::F32;
	scancode_map[0xD3] = EngineKey::F33;
	scancode_map[0xD4] = EngineKey::F34;
	scancode_map[0xD5] = EngineKey::F35;

	// Godot to scancode map.
	for (const KeyValue<unsigned int, EngineKey> &E : scancode_map) {
		scancode_map_inv[E.value] = E.key;
	}

	// Scancode to physical location map.
	// Ctrl.
	location_map[0x25] = KeyLocation::LEFT;
	location_map[0x69] = KeyLocation::RIGHT;
	// Shift.
	location_map[0x32] = KeyLocation::LEFT;
	location_map[0x3E] = KeyLocation::RIGHT;
	// Alt.
	location_map[0x40] = KeyLocation::LEFT;
	location_map[0x6C] = KeyLocation::RIGHT;
	// Meta.
	location_map[0x85] = KeyLocation::LEFT;
	location_map[0x86] = KeyLocation::RIGHT;
}

EngineKey KeyMappingXKB::get_keycode(xkb_keycode_t p_keysym) {
	if (p_keysym >= 0x20 && p_keysym < 0x7E) { // ASCII, maps 1-1
		if (p_keysym > 0x60 && p_keysym < 0x7B) { // Lowercase ASCII.
			return (EngineKey)(p_keysym - 32);
		} else {
			return (EngineKey)p_keysym;
		}
	}

	const EngineKey *key = xkb_keycode_map.getptr(p_keysym);
	if (key) {
		return *key;
	}
	return EngineKey::NONE;
}

EngineKey KeyMappingXKB::get_scancode(unsigned int p_code) {
	const EngineKey *key = scancode_map.getptr(p_code);
	if (key) {
		return *key;
	}

	return EngineKey::NONE;
}

xkb_keycode_t KeyMappingXKB::get_xkb_keycode(EngineKey p_key) {
	const unsigned int *key = scancode_map_inv.getptr(p_key);
	if (key) {
		return *key;
	}
	return 0x00;
}

KeyLocation KeyMappingXKB::get_location(unsigned int p_code) {
	const KeyLocation *location = location_map.getptr(p_code);
	if (location) {
		return *location;
	}
	return KeyLocation::UNSPECIFIED;
}
