/**************************************************************************/
/*  key_mapping_windows.cpp                                               */
/**************************************************************************/
/*                         This file is part of:                          */
/*                             GODOT ENGINE                               */
/*                        https://godotengine.org                         */
/**************************************************************************/
/* Copyright (c) 2014-present Godot Engine contributors (see AUTHORS.md). */
/* Copyright (c) 2007-2014 Juan Linietsky, Ariel Manzur.                  */
/*                                                                        */
/* Permission is hereby granted, free of charge, to any person obtaining  */
/* a copy of this software and associated documentation files (the        */
/* "Software"), to deal in the Software without restriction, including    */
/* without limitation the rights to use, copy, modify, merge, publish,    */
/* distribute, sublicense, and/or sell copies of the Software, and to     */
/* permit persons to whom the Software is furnished to do so, subject to  */
/* the following conditions:                                              */
/*                                                                        */
/* The above copyright notice and this permission notice shall be         */
/* included in all copies or substantial portions of the Software.        */
/*                                                                        */
/* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,        */
/* EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF     */
/* MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. */
/* IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY   */
/* CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,   */
/* TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE      */
/* SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.                 */
/**************************************************************************/

// This is modified from platform/windows/key_mapping_windows.cpp from Godot

#include "key_mapping_windows.h"

// This provides translation from Windows virtual key codes to Godot and back.
// See WinUser.h and the below for documentation:
// https://docs.microsoft.com/en-us/windows/win32/inputdev/virtual-key-codes

struct HashMapHasherEngineKeys {
	static _FORCE_INLINE_ uint32_t hash(const EngineKey p_key) { return hash_fmix32(static_cast<uint32_t>(p_key)); }
	static _FORCE_INLINE_ uint32_t hash(const char32_t p_uchar) { return hash_fmix32(p_uchar); }
	static _FORCE_INLINE_ uint32_t hash(const unsigned p_key) { return hash_fmix32(p_key); }
};

HashMap<unsigned int, EngineKey, HashMapHasherEngineKeys> vk_map;
HashMap<unsigned int, EngineKey, HashMapHasherEngineKeys> scansym_map;
HashMap<EngineKey, unsigned int, HashMapHasherEngineKeys> scansym_map_inv;
HashMap<unsigned int, EngineKey, HashMapHasherEngineKeys> scansym_map_ext;

void KeyMappingWindows::initialize() {
	// VK_LBUTTON (0x01)
	// VK_RBUTTON (0x02)
	// VK_CANCEL (0x03)
	// VK_MBUTTON (0x04)
	// VK_XBUTTON1 (0x05)
	// VK_XBUTTON2 (0x06), We have no mappings for the above;as we only map keyboard buttons here.
	// 0x07 is undefined.
	vk_map[VK_BACK] = EngineKey::BACKSPACE; // (0x08)
	vk_map[VK_TAB] = EngineKey::TAB; // (0x09)
	// 0x0A-0B are reserved.
	vk_map[VK_CLEAR] = EngineKey::CLEAR; // (0x0C)
	vk_map[VK_RETURN] = EngineKey::ENTER; // (0x0D)
	// 0x0E-0F are undefined.
	vk_map[VK_SHIFT] = EngineKey::SHIFT; // (0x10)
	vk_map[VK_CONTROL] = EngineKey::CTRL; // (0x11)
	vk_map[VK_MENU] = EngineKey::ALT; // (0x12)
	vk_map[VK_PAUSE] = EngineKey::PAUSE; // (0x13)
	vk_map[VK_CAPITAL] = EngineKey::CAPSLOCK; // (0x14)
	// 0x15-1A are IME keys.
	vk_map[VK_ESCAPE] = EngineKey::ESCAPE; // (0x1B)
	// 0x1C-1F are IME keys.
	vk_map[VK_SPACE] = EngineKey::SPACE; // (0x20)
	vk_map[VK_PRIOR] = EngineKey::PAGEUP; // (0x21)
	vk_map[VK_NEXT] = EngineKey::PAGEDOWN; // (0x22)
	vk_map[VK_END] = EngineKey::END; // (0x23)
	vk_map[VK_HOME] = EngineKey::HOME; // (0x24)
	vk_map[VK_LEFT] = EngineKey::LEFT; // (0x25)
	vk_map[VK_UP] = EngineKey::UP; // (0x26)
	vk_map[VK_RIGHT] = EngineKey::RIGHT; // (0x27)
	vk_map[VK_DOWN] = EngineKey::DOWN; // (0x28)
	// VK_SELECT (0x29), Old select key; e.g. on Digital Equipment Corporation keyboards.
	vk_map[VK_PRINT] = EngineKey::PRINT; // (0x2A), Old IBM key; modern keyboards use VK_SNAPSHOT.
	// VK_EXECUTE (0x2B), Old and uncommon.
	vk_map[VK_SNAPSHOT] = EngineKey::PRINT; // (0x2C)
	vk_map[VK_INSERT] = EngineKey::INSERT; // (0x2D)
	vk_map[VK_DELETE] = EngineKey::KEY_DELETE; // (0x2E)
	vk_map[VK_HELP] = EngineKey::HELP; // (0x2F)
	vk_map[0x30] = EngineKey::KEY_0; // 0 key.
	vk_map[0x31] = EngineKey::KEY_1; // 1 key.
	vk_map[0x32] = EngineKey::KEY_2; // 2 key.
	vk_map[0x33] = EngineKey::KEY_3; // 3 key.
	vk_map[0x34] = EngineKey::KEY_4; // 4 key.
	vk_map[0x35] = EngineKey::KEY_5; // 5 key.
	vk_map[0x36] = EngineKey::KEY_6; // 6 key.
	vk_map[0x37] = EngineKey::KEY_7; // 7 key.
	vk_map[0x38] = EngineKey::KEY_8; // 8 key.
	vk_map[0x39] = EngineKey::KEY_9; // 9 key.
	// 0x3A-40 are undefined.
	vk_map[0x41] = EngineKey::A; // A key.
	vk_map[0x42] = EngineKey::B; // B key.
	vk_map[0x43] = EngineKey::C; // C key.
	vk_map[0x44] = EngineKey::D; // D key.
	vk_map[0x45] = EngineKey::E; // E key.
	vk_map[0x46] = EngineKey::F; // F key.
	vk_map[0x47] = EngineKey::G; // G key.
	vk_map[0x48] = EngineKey::H; // H key.
	vk_map[0x49] = EngineKey::I; // I key
	vk_map[0x4A] = EngineKey::J; // J key.
	vk_map[0x4B] = EngineKey::K; // K key.
	vk_map[0x4C] = EngineKey::L; // L key.
	vk_map[0x4D] = EngineKey::M; // M key.
	vk_map[0x4E] = EngineKey::N; // N key.
	vk_map[0x4F] = EngineKey::O; // O key.
	vk_map[0x50] = EngineKey::P; // P key.
	vk_map[0x51] = EngineKey::Q; // Q key.
	vk_map[0x52] = EngineKey::R; // R key.
	vk_map[0x53] = EngineKey::S; // S key.
	vk_map[0x54] = EngineKey::T; // T key.
	vk_map[0x55] = EngineKey::U; // U key.
	vk_map[0x56] = EngineKey::V; // V key.
	vk_map[0x57] = EngineKey::W; // W key.
	vk_map[0x58] = EngineKey::X; // X key.
	vk_map[0x59] = EngineKey::Y; // Y key.
	vk_map[0x5A] = EngineKey::Z; // Z key.
	vk_map[VK_LWIN] = (EngineKey)EngineKey::META; // (0x5B)
	vk_map[VK_RWIN] = (EngineKey)EngineKey::META; // (0x5C)
	vk_map[VK_APPS] = EngineKey::MENU; // (0x5D)
	// 0x5E is reserved.
	vk_map[VK_SLEEP] = EngineKey::STANDBY; // (0x5F)
	vk_map[VK_NUMPAD0] = EngineKey::KP_0; // (0x60)
	vk_map[VK_NUMPAD1] = EngineKey::KP_1; // (0x61)
	vk_map[VK_NUMPAD2] = EngineKey::KP_2; // (0x62)
	vk_map[VK_NUMPAD3] = EngineKey::KP_3; // (0x63)
	vk_map[VK_NUMPAD4] = EngineKey::KP_4; // (0x64)
	vk_map[VK_NUMPAD5] = EngineKey::KP_5; // (0x65)
	vk_map[VK_NUMPAD6] = EngineKey::KP_6; // (0x66)
	vk_map[VK_NUMPAD7] = EngineKey::KP_7; // (0x67)
	vk_map[VK_NUMPAD8] = EngineKey::KP_8; // (0x68)
	vk_map[VK_NUMPAD9] = EngineKey::KP_9; // (0x69)
	vk_map[VK_MULTIPLY] = EngineKey::KP_MULTIPLY; // (0x6A)
	vk_map[VK_ADD] = EngineKey::KP_ADD; // (0x6B)
	vk_map[VK_SEPARATOR] = EngineKey::KP_PERIOD; // (0x6C)
	vk_map[VK_SUBTRACT] = EngineKey::KP_SUBTRACT; // (0x6D)
	vk_map[VK_DECIMAL] = EngineKey::KP_PERIOD; // (0x6E)
	vk_map[VK_DIVIDE] = EngineKey::KP_DIVIDE; // (0x6F)
	vk_map[VK_F1] = EngineKey::F1; // (0x70)
	vk_map[VK_F2] = EngineKey::F2; // (0x71)
	vk_map[VK_F3] = EngineKey::F3; // (0x72)
	vk_map[VK_F4] = EngineKey::F4; // (0x73)
	vk_map[VK_F5] = EngineKey::F5; // (0x74)
	vk_map[VK_F6] = EngineKey::F6; // (0x75)
	vk_map[VK_F7] = EngineKey::F7; // (0x76)
	vk_map[VK_F8] = EngineKey::F8; // (0x77)
	vk_map[VK_F9] = EngineKey::F9; // (0x78)
	vk_map[VK_F10] = EngineKey::F10; // (0x79)
	vk_map[VK_F11] = EngineKey::F11; // (0x7A)
	vk_map[VK_F12] = EngineKey::F12; // (0x7B)
	vk_map[VK_F13] = EngineKey::F13; // (0x7C)
	vk_map[VK_F14] = EngineKey::F14; // (0x7D)
	vk_map[VK_F15] = EngineKey::F15; // (0x7E)
	vk_map[VK_F16] = EngineKey::F16; // (0x7F)
	vk_map[VK_F17] = EngineKey::F17; // (0x80)
	vk_map[VK_F18] = EngineKey::F18; // (0x81)
	vk_map[VK_F19] = EngineKey::F19; // (0x82)
	vk_map[VK_F20] = EngineKey::F20; // (0x83)
	vk_map[VK_F21] = EngineKey::F21; // (0x84)
	vk_map[VK_F22] = EngineKey::F22; // (0x85)
	vk_map[VK_F23] = EngineKey::F23; // (0x86)
	vk_map[VK_F24] = EngineKey::F24; // (0x87)
	// 0x88-8F are reserved for UI navigation.
	vk_map[VK_NUMLOCK] = EngineKey::NUMLOCK; // (0x90)
	vk_map[VK_SCROLL] = EngineKey::SCROLLLOCK; // (0x91)
	vk_map[VK_OEM_NEC_EQUAL] = EngineKey::EQUAL; // (0x92), OEM NEC PC-9800 numpad '=' key.
	// 0x93-96 are OEM specific (e.g. used by Fujitsu/OASYS);
	// 0x97-9F are unassigned.
	vk_map[VK_LSHIFT] = EngineKey::SHIFT; // (0xA0)
	vk_map[VK_RSHIFT] = EngineKey::SHIFT; // (0xA1)
	vk_map[VK_LCONTROL] = EngineKey::CTRL; // (0xA2)
	vk_map[VK_RCONTROL] = EngineKey::CTRL; // (0xA3)
	vk_map[VK_LMENU] = EngineKey::MENU; // (0xA4)
	vk_map[VK_RMENU] = EngineKey::MENU; // (0xA5)
	vk_map[VK_BROWSER_BACK] = EngineKey::BACK; // (0xA6)
	vk_map[VK_BROWSER_FORWARD] = EngineKey::FORWARD; // (0xA7)
	vk_map[VK_BROWSER_REFRESH] = EngineKey::REFRESH; // (0xA8)
	vk_map[VK_BROWSER_STOP] = EngineKey::STOP; // (0xA9)
	vk_map[VK_BROWSER_SEARCH] = EngineKey::SEARCH; // (0xAA)
	vk_map[VK_BROWSER_FAVORITES] = EngineKey::FAVORITES; // (0xAB)
	vk_map[VK_BROWSER_HOME] = EngineKey::HOMEPAGE; // (0xAC)
	vk_map[VK_VOLUME_MUTE] = EngineKey::VOLUMEMUTE; // (0xAD)
	vk_map[VK_VOLUME_DOWN] = EngineKey::VOLUMEDOWN; // (0xAE)
	vk_map[VK_VOLUME_UP] = EngineKey::VOLUMEUP; // (0xAF)
	vk_map[VK_MEDIA_NEXT_TRACK] = EngineKey::MEDIANEXT; // (0xB0)
	vk_map[VK_MEDIA_PREV_TRACK] = EngineKey::MEDIAPREVIOUS; // (0xB1)
	vk_map[VK_MEDIA_STOP] = EngineKey::MEDIASTOP; // (0xB2)
	vk_map[VK_MEDIA_PLAY_PAUSE] = EngineKey::MEDIAPLAY; // (0xB3), Media button play/pause toggle.
	vk_map[VK_LAUNCH_MAIL] = EngineKey::LAUNCHMAIL; // (0xB4)
	vk_map[VK_LAUNCH_MEDIA_SELECT] = EngineKey::LAUNCHMEDIA; // (0xB5)
	vk_map[VK_LAUNCH_APP1] = EngineKey::LAUNCH0; // (0xB6)
	vk_map[VK_LAUNCH_APP2] = EngineKey::LAUNCH1; // (0xB7)
	// 0xB8-B9 are reserved.
	vk_map[VK_OEM_1] = EngineKey::SEMICOLON; // (0xBA), Misc. character;can vary by keyboard/region. For US standard keyboards;the ';:' key.
	vk_map[VK_OEM_PLUS] = EngineKey::EQUAL; // (0xBB)
	vk_map[VK_OEM_COMMA] = EngineKey::COMMA; // (0xBC)
	vk_map[VK_OEM_MINUS] = EngineKey::MINUS; // (0xBD)
	vk_map[VK_OEM_PERIOD] = EngineKey::PERIOD; // (0xBE)
	vk_map[VK_OEM_2] = EngineKey::SLASH; // (0xBF), For US standard keyboards;the '/?' key.
	vk_map[VK_OEM_3] = EngineKey::QUOTELEFT; // (0xC0), For US standard keyboards;the '`~' key.
	// 0xC1-D7 are reserved. 0xD8-DA are unassigned.
	// 0xC3-DA may be used for old gamepads? Maybe we want to support this? See WinUser.h.
	vk_map[VK_OEM_4] = EngineKey::BRACKETLEFT; // (0xDB),  For US standard keyboards;the '[{' key.
	vk_map[VK_OEM_5] = EngineKey::BACKSLASH; // (0xDC), For US standard keyboards;the '\|' key.
	vk_map[VK_OEM_6] = EngineKey::BRACKETRIGHT; // (0xDD), For US standard keyboards;the ']}' key.
	vk_map[VK_OEM_7] = EngineKey::APOSTROPHE; // (0xDE), For US standard keyboards;single quote/double quote.
	// VK_OEM_8 (0xDF)
	// 0xE0 is reserved. 0xE1 is OEM specific.
	vk_map[VK_OEM_102] = EngineKey::BAR; // (0xE2), Either angle bracket or backslash key on the RT 102-key keyboard.
	vk_map[VK_ICO_HELP] = EngineKey::HELP; // (0xE3)
	// 0xE4 is OEM (e.g. ICO) specific.
	// VK_PROCESSKEY (0xE5), For IME.
	vk_map[VK_ICO_CLEAR] = EngineKey::CLEAR; // (0xE6)
	// VK_PACKET (0xE7), Used to pass Unicode characters as if they were keystrokes.
	// 0xE8 is unassigned.
	// 0xE9-F5 are OEM (Nokia/Ericsson) specific.
	vk_map[VK_ATTN] = EngineKey::ESCAPE; // (0xF6), Old IBM 'ATTN' key used on midrange computers ;e.g. AS/400.
	vk_map[VK_CRSEL] = EngineKey::TAB; // (0xF7), Old IBM 3270 'CrSel' (cursor select) key; used to select data fields.
	// VK_EXSEL (0xF7), Old IBM 3270 extended selection key.
	// VK_EREOF (0xF8), Old IBM 3270 erase to end of field key.
	vk_map[VK_PLAY] = EngineKey::MEDIAPLAY; // (0xFA), Old IBM 3270 'Play' key.
	// VK_ZOOM (0xFB), Old IBM 3290 'Zoom' key.
	// VK_NONAME (0xFC), Reserved.
	// VK_PA1 (0xFD), Old IBM 3270 PA1 key.
	vk_map[VK_OEM_CLEAR] = EngineKey::CLEAR; // (0xFE), OEM specific clear key. Unclear how it differs from normal clear.

	scansym_map[0x00] = EngineKey::PAUSE;
	scansym_map[0x01] = EngineKey::ESCAPE;
	scansym_map[0x02] = EngineKey::KEY_1;
	scansym_map[0x03] = EngineKey::KEY_2;
	scansym_map[0x04] = EngineKey::KEY_3;
	scansym_map[0x05] = EngineKey::KEY_4;
	scansym_map[0x06] = EngineKey::KEY_5;
	scansym_map[0x07] = EngineKey::KEY_6;
	scansym_map[0x08] = EngineKey::KEY_7;
	scansym_map[0x09] = EngineKey::KEY_8;
	scansym_map[0x0A] = EngineKey::KEY_9;
	scansym_map[0x0B] = EngineKey::KEY_0;
	scansym_map[0x0C] = EngineKey::MINUS;
	scansym_map[0x0D] = EngineKey::EQUAL;
	scansym_map[0x0E] = EngineKey::BACKSPACE;
	scansym_map[0x0F] = EngineKey::TAB;
	scansym_map[0x10] = EngineKey::Q;
	scansym_map[0x11] = EngineKey::W;
	scansym_map[0x12] = EngineKey::E;
	scansym_map[0x13] = EngineKey::R;
	scansym_map[0x14] = EngineKey::T;
	scansym_map[0x15] = EngineKey::Y;
	scansym_map[0x16] = EngineKey::U;
	scansym_map[0x17] = EngineKey::I;
	scansym_map[0x18] = EngineKey::O;
	scansym_map[0x19] = EngineKey::P;
	scansym_map[0x1A] = EngineKey::BRACKETLEFT;
	scansym_map[0x1B] = EngineKey::BRACKETRIGHT;
	scansym_map[0x1C] = EngineKey::ENTER;
	scansym_map[0x1D] = EngineKey::CTRL;
	scansym_map[0x1E] = EngineKey::A;
	scansym_map[0x1F] = EngineKey::S;
	scansym_map[0x20] = EngineKey::D;
	scansym_map[0x21] = EngineKey::F;
	scansym_map[0x22] = EngineKey::G;
	scansym_map[0x23] = EngineKey::H;
	scansym_map[0x24] = EngineKey::J;
	scansym_map[0x25] = EngineKey::K;
	scansym_map[0x26] = EngineKey::L;
	scansym_map[0x27] = EngineKey::SEMICOLON;
	scansym_map[0x28] = EngineKey::APOSTROPHE;
	scansym_map[0x29] = EngineKey::QUOTELEFT;
	scansym_map[0x2A] = EngineKey::SHIFT;
	scansym_map[0x2B] = EngineKey::BACKSLASH;
	scansym_map[0x2C] = EngineKey::Z;
	scansym_map[0x2D] = EngineKey::X;
	scansym_map[0x2E] = EngineKey::C;
	scansym_map[0x2F] = EngineKey::V;
	scansym_map[0x30] = EngineKey::B;
	scansym_map[0x31] = EngineKey::N;
	scansym_map[0x32] = EngineKey::M;
	scansym_map[0x33] = EngineKey::COMMA;
	scansym_map[0x34] = EngineKey::PERIOD;
	scansym_map[0x35] = EngineKey::SLASH;
	scansym_map[0x36] = EngineKey::SHIFT;
	scansym_map[0x37] = EngineKey::KP_MULTIPLY;
	scansym_map[0x38] = EngineKey::ALT;
	scansym_map[0x39] = EngineKey::SPACE;
	scansym_map[0x3A] = EngineKey::CAPSLOCK;
	scansym_map[0x3B] = EngineKey::F1;
	scansym_map[0x3C] = EngineKey::F2;
	scansym_map[0x3D] = EngineKey::F3;
	scansym_map[0x3E] = EngineKey::F4;
	scansym_map[0x3F] = EngineKey::F5;
	scansym_map[0x40] = EngineKey::F6;
	scansym_map[0x41] = EngineKey::F7;
	scansym_map[0x42] = EngineKey::F8;
	scansym_map[0x43] = EngineKey::F9;
	scansym_map[0x44] = EngineKey::F10;
	scansym_map[0x45] = EngineKey::NUMLOCK;
	scansym_map[0x46] = EngineKey::SCROLLLOCK;
	scansym_map[0x47] = EngineKey::KP_7;
	scansym_map[0x48] = EngineKey::KP_8;
	scansym_map[0x49] = EngineKey::KP_9;
	scansym_map[0x4A] = EngineKey::KP_SUBTRACT;
	scansym_map[0x4B] = EngineKey::KP_4;
	scansym_map[0x4C] = EngineKey::KP_5;
	scansym_map[0x4D] = EngineKey::KP_6;
	scansym_map[0x4E] = EngineKey::KP_ADD;
	scansym_map[0x4F] = EngineKey::KP_1;
	scansym_map[0x50] = EngineKey::KP_2;
	scansym_map[0x51] = EngineKey::KP_3;
	scansym_map[0x52] = EngineKey::KP_0;
	scansym_map[0x53] = EngineKey::KP_PERIOD;
	scansym_map[0x56] = EngineKey::SECTION;
	scansym_map[0x57] = EngineKey::F11;
	scansym_map[0x58] = EngineKey::F12;
	scansym_map[0x5B] = EngineKey::META;
	scansym_map[0x5C] = EngineKey::META;
	scansym_map[0x5D] = EngineKey::MENU;
	scansym_map[0x64] = EngineKey::F13;
	scansym_map[0x65] = EngineKey::F14;
	scansym_map[0x66] = EngineKey::F15;
	scansym_map[0x67] = EngineKey::F16;
	scansym_map[0x68] = EngineKey::F17;
	scansym_map[0x69] = EngineKey::F18;
	scansym_map[0x6A] = EngineKey::F19;
	scansym_map[0x6B] = EngineKey::F20;
	scansym_map[0x6C] = EngineKey::F21;
	scansym_map[0x6D] = EngineKey::F22;
	scansym_map[0x6E] = EngineKey::F23;
	//	scansym_map[0x71] = EngineKey::JIS_KANA;
	//	scansym_map[0x72] = EngineKey::JIS_EISU;
	scansym_map[0x76] = EngineKey::F24;

	for (const KeyValue<unsigned int, EngineKey> &E : scansym_map) {
		scansym_map_inv[E.value] = E.key;
	}

	scansym_map_ext[0x09] = EngineKey::MENU;
	scansym_map_ext[0x10] = EngineKey::MEDIAPREVIOUS;
	scansym_map_ext[0x19] = EngineKey::MEDIANEXT;
	scansym_map_ext[0x1C] = EngineKey::KP_ENTER;
	scansym_map_ext[0x20] = EngineKey::VOLUMEMUTE;
	scansym_map_ext[0x21] = EngineKey::LAUNCH1;
	scansym_map_ext[0x22] = EngineKey::MEDIAPLAY;
	scansym_map_ext[0x24] = EngineKey::MEDIASTOP;
	scansym_map_ext[0x2E] = EngineKey::VOLUMEDOWN;
	scansym_map_ext[0x30] = EngineKey::VOLUMEUP;
	scansym_map_ext[0x32] = EngineKey::HOMEPAGE;
	scansym_map_ext[0x35] = EngineKey::KP_DIVIDE;
	scansym_map_ext[0x37] = EngineKey::PRINT;
	scansym_map_ext[0x3A] = EngineKey::KP_ADD;
	scansym_map_ext[0x45] = EngineKey::NUMLOCK;
	scansym_map_ext[0x47] = EngineKey::HOME;
	scansym_map_ext[0x48] = EngineKey::UP;
	scansym_map_ext[0x49] = EngineKey::PAGEUP;
	scansym_map_ext[0x4A] = EngineKey::KP_SUBTRACT;
	scansym_map_ext[0x4B] = EngineKey::LEFT;
	scansym_map_ext[0x4C] = EngineKey::KP_5;
	scansym_map_ext[0x4D] = EngineKey::RIGHT;
	scansym_map_ext[0x4E] = EngineKey::KP_ADD;
	scansym_map_ext[0x4F] = EngineKey::END;
	scansym_map_ext[0x50] = EngineKey::DOWN;
	scansym_map_ext[0x51] = EngineKey::PAGEDOWN;
	scansym_map_ext[0x52] = EngineKey::INSERT;
	scansym_map_ext[0x53] = EngineKey::KEY_DELETE;
	scansym_map_ext[0x5D] = EngineKey::MENU;
	scansym_map_ext[0x5F] = EngineKey::STANDBY;
	scansym_map_ext[0x65] = EngineKey::SEARCH;
	scansym_map_ext[0x66] = EngineKey::FAVORITES;
	scansym_map_ext[0x67] = EngineKey::REFRESH;
	scansym_map_ext[0x68] = EngineKey::STOP;
	scansym_map_ext[0x69] = EngineKey::FORWARD;
	scansym_map_ext[0x6A] = EngineKey::BACK;
	scansym_map_ext[0x6B] = EngineKey::LAUNCH0;
	scansym_map_ext[0x6C] = EngineKey::LAUNCHMAIL;
	scansym_map_ext[0x6D] = EngineKey::LAUNCHMEDIA;
	scansym_map_ext[0x78] = EngineKey::MEDIARECORD;
}

EngineKey KeyMappingWindows::get_keysym(unsigned int p_code) {
	const EngineKey *key = vk_map.getptr(p_code);
	if (key) {
		return *key;
	}
	return EngineKey::UNKNOWN;
}

unsigned int KeyMappingWindows::get_scancode(EngineKey p_keycode) {
	const unsigned int *key = scansym_map_inv.getptr(p_keycode);
	if (key) {
		return *key;
	}
	return 0;
}

EngineKey KeyMappingWindows::get_scansym(unsigned int p_code, bool p_extended) {
	if (p_extended) {
		const EngineKey *key = scansym_map_ext.getptr(p_code);
		if (key) {
			return *key;
		}
	}
	const EngineKey *key = scansym_map.getptr(p_code);
	if (key) {
		return *key;
	}
	return EngineKey::NONE;
}

bool KeyMappingWindows::is_extended_key(unsigned int p_code) {
	return p_code == VK_INSERT ||
			p_code == VK_DELETE ||
			p_code == VK_HOME ||
			p_code == VK_END ||
			p_code == VK_PRIOR ||
			p_code == VK_NEXT ||
			p_code == VK_LEFT ||
			p_code == VK_UP ||
			p_code == VK_RIGHT ||
			p_code == VK_DOWN;
}
